#A module containg functions that implement a tic tac toe AI

#Given the piece number, find the string representation
def decodePiece(piece):
	if piece == 1:
		return "X"
	elif piece == -1:
		return "O"
	else:
		return " "

#Display the board as an ASCII art kind of thing
def printBoard(board):
	print "+-+-+-+"
	print "|" + decodePiece(board[0]) + "|" + decodePiece(board[1]) + "|" + decodePiece(board[2]) + "|"
	print "+-+-+-+"
	print "|" + decodePiece(board[3]) + "|" + decodePiece(board[4]) + "|" + decodePiece(board[5]) + "|"
	print "+-+-+-+"
	print "|" + decodePiece(board[6]) + "|" + decodePiece(board[7]) + "|" + decodePiece(board[8]) + "|"
	print "+-+-+-+"
	print "**************************************"

#A generator that generates a board for every possible move by player
def generateMoves(board, player):
	temp = board[:] #Use a copy so as not to contaminate the board
	for i in range(9):
		if temp[i] == 0:
			temp[i] = player
			yield temp
			temp[i] = 0

#Check to see if the game has been won
def checkWin(board):
    if board[0] == board[1] and board[1] == board[2] and board[0] != 0:
        return board[0]
    if board[3] == board[4] and board[4] == board[5] and board[3] != 0:
        return board[3]
    if board[6] == board[7] and board[7] == board[8] and board[6] != 0:
        return board[6]

    if board[0] == board[3] and board[3] == board[6] and board[0] != 0:
        return board[0]
    if board[1] == board[4] and board[4] == board[7] and board[1] != 0:
        return board[1]
    if board[2] == board[5] and board[5] == board[8] and board[2] != 0:
        return board[2]

    if board[0] == board[4] and board[4] == board[8] and board[0] != 0:
        return board[0]
    if board[2] == board[4] and board[4] == board[6] and board[2] != 0:
        return board[2]

    return 0

#Check to see if there is a tie. Should only be called if it is known the game
#hasn't been won
def checkTie(board):
    #If the board is full, and no one has won, then it must be a tie
    for spot in board:
        if spot == 0: return False

    return True

#The main negamax algorithm. Only returns scores, use firstNegamax() to find the
#best move
def negamax(board, player):
	win = checkWin(board)
	if win != 0:
        #Always return -1 on a win because the winning move was by the other
        #person
		return -1
	elif checkTie(board):
		return 0

    #Recurse through the tree, looking for one with the best end node
	bestVal = -2
	for move in generateMoves(board, player):
		val = -negamax(move, -player)
		if val >= bestVal:
			bestVal = val

	return bestVal

#This function returns the best next move. It is the first level of recursion of
#negamax() pulled out so only it has to keep track of moves, and negamax()
#doesn't
def firstNegamax(board, player):
	bestVal = -2
	bestMove = board

    #Find the move with the highest value, and make that move
	for move in generateMoves(board, player):
		val = -negamax(move, -player)
		if val >= bestVal:
            #Make a copy of the move so that changes made by the generator don't
            #affect this
			bestMove = move[:]
			bestVal = val

	return bestMove