#!/usr/bin/python

#Import all of the tic tac toe functions
from tictactoe import *

#Let the user pick the number of people playing. Leftover people will be filled
#up with AI
def pickNumPlayers():
    while True:
        try:
            choice = int(raw_input("Players: ").strip())
        except ValueError:
            print "Try again you idiot"
            continue

        if choice < 0 or choice > 2:
            print "Try again you idiot" #Insult the person if they can't type
        else:
            return choice

#When playing against the computer, the player can decide what letter they get
def playerLetterChoice():
	while True:
		choice = raw_input("Your letter: ").strip().upper()[0]
		if choice == "X":
			return 1
		elif choice == "O":
			return -1
		else:
			print "Try again you idiot" #Insult the person if they can't type

#Get the player's spot (0 - 8)
def playerPickSpot(board):
    while True:
        try:
            choice = int(raw_input("Pick a spot: ").strip())
        except ValueError:
            print "Try again you idiot" #Insult the person if they can't type
            continue

        if choice < 0 or choice > 8:
            print "Try again you idiot" #Insult the person if they can't type
        elif board[choice] != 0:
            print "That spot is already taken, try again"
        else:
            return choice

#Print the result of the game. Should only be called after the game is over
def printResults(board):
    win = checkWin(board)
    if win == 1:
        print "X Wins"
    elif win == -1:
        print "O Wins"
    else:
        print "Tie"

#Let the computer play against itself. Very boring, ends in tie
def compVcomp():
    board = [0,0,0, 0,0,0, 0,0,0] #Initialize the board

    player = 1
    while checkWin(board) == 0 and not checkTie(board): #While the game is going
        board = firstNegamax(board, player) #Let the computer play
        printBoard(board)
        player *= -1 #Switch players

    printResults(board)

#Player vs. Computer. Much more interesting
def compVman():
    board = [0,0,0, 0,0,0, 0,0,0] #Initialize the board
    playerLetter = playerLetterChoice() #Let the player pick their letter

    #Show a blank board for the player if they go first
    if playerLetter == 1: printBoard(board)

    player = 1
    while checkWin(board) == 0 and not checkTie(board): #While the game is going
        if player == playerLetter: #If its the player's turn, let him pick
            board[playerPickSpot(board)] = player
        else: #Otherwise let the computer go
            board = firstNegamax(board, player)

        printBoard(board)
        player *= -1 #Switch Players

    printResults(board)

#Two player game
def manVman():
    board = [0,0,0, 0,0,0, 0,0,0] #Initialize the board

    printBoard(board) #Print a blank board for the first player

    player = 1
    while checkWin(board) == 0 and not checkTie(board):
        board[playerPickSpot(board)] = player #Let the person play
        printBoard(board)
        player *= -1 #Switch players

    printResults(board)

def main():
    numPlayers = pickNumPlayers()

    #Goto the chosen game mode
    if numPlayers == 0:
        compVcomp()
    elif numPlayers == 1:
        compVman()
    elif numPlayers == 2:
        manVman()

#Start the game
if __name__ == "__main__": main()