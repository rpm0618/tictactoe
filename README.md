Tic Tac Toe
=========================================
Ryan McVeety

This is a console version of tic tac toe implemented in python.
To run, type "python game.py", and follow the on screen prompts.
There is a built in AI based on a variant of minimax called
negamax.

Positions on board (used when selecting moves):
+-+-+-+
|0|1|2|
+-+-+-+
|3|4|5|
+-+-+-+
|6|7|8|
+-+-+-+